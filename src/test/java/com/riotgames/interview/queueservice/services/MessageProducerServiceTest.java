package com.riotgames.interview.queueservice.services;

import static org.mockito.Mockito.*;

import com.riotgames.interview.queueservice.dtos.QueueRequestMessageDto;
import com.riotgames.interview.queueservice.enums.GameType;
import com.riotgames.interview.queueservice.enums.Rank;
import java.time.OffsetDateTime;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;
import reactor.core.publisher.Mono;
import reactor.kafka.sender.SenderResult;
import reactor.test.StepVerifier;

class MessageProducerServiceTest {

  public static final String TESTTOPIC = "testtopic";

  @Mock
  private ReactiveKafkaProducerTemplate<String, QueueRequestMessageDto>
      reactiveKafkaProducerTemplateMock;

  private MessageProducerService messageProducerService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
    messageProducerService =
        new MessageProducerService(reactiveKafkaProducerTemplateMock, TESTTOPIC);
  }

  @AfterEach
  void tearDown() {
    reactiveKafkaProducerTemplateMock.close();
  }

  @Test
  void testSendMessage() {
    QueueRequestMessageDto messageStub =
        QueueRequestMessageDto.builder()
            .userId("testUserId")
            .queueId("testQueueId")
            .rank(Rank.BRONZE)
            .gameType(GameType._3V3)
            .createdDateTime(OffsetDateTime.now())
            .build();

    SenderResult<Void> senderResult = mock(SenderResult.class);
    when(reactiveKafkaProducerTemplateMock.send(any(), any(), any()))
        .thenReturn(Mono.just(senderResult));

    Mono<Void> result = messageProducerService.send(messageStub);

    StepVerifier.create(result).verifyComplete();

    verify(reactiveKafkaProducerTemplateMock)
        .send(eq(TESTTOPIC), eq(messageStub.getUserId()), eq(messageStub));
  }
}
