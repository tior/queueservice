package com.riotgames.interview.queueservice.services;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

import com.riotgames.interview.queueservice.dtos.QueueCreatedResponsePayload;
import com.riotgames.interview.queueservice.dtos.QueueRequestMessageDto;
import com.riotgames.interview.queueservice.dtos.QueueRequestPayload;
import com.riotgames.interview.queueservice.dtos.QueueStatusResponsePayload;
import com.riotgames.interview.queueservice.enums.GameType;
import com.riotgames.interview.queueservice.enums.QueueStatus;
import com.riotgames.interview.queueservice.enums.Rank;
import com.riotgames.interview.queueservice.mappers.GlobalMapper;
import com.riotgames.interview.queueservice.models.Queue;
import com.riotgames.interview.queueservice.repositories.QueuesRepository;
import java.time.Duration;
import java.time.OffsetDateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

class QueueServiceTest {

  @Mock private QueuesRepository queuesRepositoryMock;

  @Mock private MessageProducerService messageProducerServiceMock;

  @Mock private GlobalMapper globalMapperMock;

  private QueueService queueServiceUnderTest;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
    queueServiceUnderTest =
        new QueueService(queuesRepositoryMock, messageProducerServiceMock, globalMapperMock);
  }

  @Test
  void testCreateQueue() {
    QueueRequestPayload requestPayload =
        QueueRequestPayload.builder()
            .userId("testUserId")
            .rank(Rank.BRONZE)
            .gameType(GameType._3V3)
            .build();

    QueueRequestMessageDto messageDto =
        QueueRequestMessageDto.builder()
            .userId("testUserId")
            .rank(Rank.BRONZE)
            .gameType(GameType._3V3)
            .build();
    QueueCreatedResponsePayload responsePayload =
        QueueCreatedResponsePayload.builder()
            .userId("testUserId")
            .queueId("testQueueId")
            .rank(Rank.BRONZE)
            .gameType(GameType._3V3)
            .createdDateTime(OffsetDateTime.now())
            .build();

    when(globalMapperMock.queueRequestPayloadToqueueRequestMessageDto(requestPayload))
        .thenReturn(messageDto);
    when(globalMapperMock.queueRequestMessageDtoToQueueCreatedResponsePayload(messageDto))
        .thenReturn(responsePayload);
    when(globalMapperMock.queueRequestMessageDtoToQueueWithFindingState(messageDto))
        .thenCallRealMethod();

    when(queuesRepositoryMock.getUserQueue(messageDto.getUserId())).thenReturn(Mono.empty());
    when(queuesRepositoryMock.save(any(Queue.class), eq(Duration.ofHours(2))))
        .thenReturn(Mono.just(true));
    when(messageProducerServiceMock.send(messageDto)).thenReturn(Mono.empty());

    StepVerifier.create(queueServiceUnderTest.createQueue(requestPayload))
        .expectNext(responsePayload)
        .verifyComplete();

    verify(queuesRepositoryMock).getUserQueue(messageDto.getUserId());
    verify(queuesRepositoryMock).save(any(Queue.class), eq(Duration.ofHours(2)));
    verify(messageProducerServiceMock).send(messageDto);
  }

  @Test
  void testGetQueueStatus() {
    String queueId = "testGetQueueStatus";

    Queue queue = Queue.builder().queueId(queueId).build();
    QueueStatusResponsePayload responsePayload =
        QueueStatusResponsePayload.builder()
            .queueId(queueId)
            .queueStatus(QueueStatus.FINDING)
            .build();

    when(queuesRepositoryMock.get(queueId)).thenReturn(Mono.just(queue));
    when(globalMapperMock.queueToQueueStatusResponsePayload(any(Queue.class)))
        .thenReturn(responsePayload);
    StepVerifier.create(queueServiceUnderTest.getQueueStatus(queueId))
        .expectNext(responsePayload)
        .verifyComplete();

    verify(queuesRepositoryMock).get(queueId);
  }

  @Test
  void testDeleteQueue() {
    String queueId = "testDeleteQueue";

    when(queuesRepositoryMock.delete(queueId)).thenReturn(Mono.just(true));

    StepVerifier.create(queueServiceUnderTest.deleteQueue(queueId))
        .expectNext(true)
        .verifyComplete();

    verify(queuesRepositoryMock).delete(queueId);
  }
}
