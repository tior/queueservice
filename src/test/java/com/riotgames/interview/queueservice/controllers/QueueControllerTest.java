package com.riotgames.interview.queueservice.controllers;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.riotgames.interview.queueservice.dtos.QueueCreatedResponsePayload;
import com.riotgames.interview.queueservice.dtos.QueueRequestPayload;
import com.riotgames.interview.queueservice.dtos.QueueStatusResponsePayload;
import com.riotgames.interview.queueservice.enums.QueueStatus;
import com.riotgames.interview.queueservice.exceptions.QueueNotFoundException;
import com.riotgames.interview.queueservice.services.QueueService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

class QueueControllerTest {

  private QueueController queueControllerUnderTest;
  @Mock private QueueService queueServiceMock;
  AutoCloseable closeable;

  @BeforeEach
  void setup() {
    closeable = MockitoAnnotations.openMocks(this);
    queueControllerUnderTest = new QueueController(queueServiceMock);
  }

  @AfterEach
  void tearDown() throws Exception {
    closeable.close();
  }

  @Test
  public void createQueueRequestTest() {
    QueueRequestPayload requestPayload = QueueRequestPayload.builder().userId("user1").build();
    QueueCreatedResponsePayload responsePayload =
        QueueCreatedResponsePayload.builder().userId("user1").queueId("queue1").build();

    when(queueServiceMock.createQueue(requestPayload)).thenReturn(Mono.just(responsePayload));

    Mono<QueueRequestPayload> requestMono = Mono.just(requestPayload);

    StepVerifier.create(queueControllerUnderTest.createQueueRequest(requestMono))
        .expectNextMatches(
            response ->
                response.getUserId().equals("user1") && response.getQueueId().equals("queue1"))
        .verifyComplete();

    verify(queueServiceMock, times(1)).createQueue(requestPayload);
  }

  @Test
  void getQueueStatusTest() {
    String queueId = "queue1";
    QueueStatusResponsePayload responsePayload =
        QueueStatusResponsePayload.builder()
            .queueId(queueId)
            .queueStatus(QueueStatus.FINDING)
            .build();
    when(queueServiceMock.getQueueStatus(queueId)).thenReturn(Mono.just(responsePayload));

    StepVerifier.create(queueControllerUnderTest.getQueueStatus(queueId))
        .expectNextMatches(
            response ->
                response.getQueueId().equals(queueId)
                    && response.getQueueStatus().equals(QueueStatus.FINDING))
        .verifyComplete();

    verify(queueServiceMock, times(1)).getQueueStatus(queueId);
  }

  @Test
  void getQueueStatusQueueNotFoundExceptionTest() {
    String queueId = "fake-123";
    when(queueServiceMock.getQueueStatus(queueId))
        .thenReturn(Mono.error(new QueueNotFoundException("fake-123")));

    StepVerifier.create(queueControllerUnderTest.getQueueStatus(queueId))
        .expectErrorSatisfies(
            throwable -> {
              assertTrue(throwable instanceof ResponseStatusException);
              ResponseStatusException exception = (ResponseStatusException) throwable;
              assertEquals(HttpStatus.NOT_FOUND, exception.getStatusCode());
              assertEquals("QueueId not found", exception.getReason());
            })
        .verify();

    verify(queueServiceMock, times(1)).getQueueStatus(queueId);
  }

  @Test
  void cancelQueueRequestTest() {
    String queueId = "queue1";
    when(queueServiceMock.deleteQueue(queueId)).thenReturn(Mono.just(true));

    StepVerifier.create(queueControllerUnderTest.cancelQueueRequest(queueId))
        .expectNext(true)
        .verifyComplete();

    verify(queueServiceMock, times(1)).deleteQueue(queueId);
  }
}
