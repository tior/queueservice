package com.riotgames.interview.queueservice.services;

import com.riotgames.interview.queueservice.dtos.QueueCreatedResponsePayload;
import com.riotgames.interview.queueservice.dtos.QueueRequestMessageDto;
import com.riotgames.interview.queueservice.dtos.QueueRequestPayload;
import com.riotgames.interview.queueservice.dtos.QueueStatusResponsePayload;
import com.riotgames.interview.queueservice.enums.QueueStatus;
import com.riotgames.interview.queueservice.exceptions.QueueNotFoundException;
import com.riotgames.interview.queueservice.exceptions.UserNotAvailableForQueueException;
import com.riotgames.interview.queueservice.mappers.GlobalMapper;
import com.riotgames.interview.queueservice.repositories.QueuesRepository;
import java.time.Duration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class QueueService {

  public static final Duration DEFAULT_TTL = Duration.ofHours(2);
  private final QueuesRepository queuesRepository;
  private final MessageProducerService messageProducerService;
  private final GlobalMapper globalMapper;

  public QueueService(
      @Autowired QueuesRepository queuesRepository,
      @Autowired MessageProducerService messageProducerService,
      @Autowired GlobalMapper globalMapper) {
    this.queuesRepository = queuesRepository;
    this.messageProducerService = messageProducerService;
    this.globalMapper = globalMapper;
  }

  public Mono<QueueCreatedResponsePayload> createQueue(QueueRequestPayload queueRequestPayload) {
    QueueRequestMessageDto queueRequestMessageDto =
        globalMapper.queueRequestPayloadToqueueRequestMessageDto(queueRequestPayload);
    return queuesRepository
        .getUserQueue(queueRequestMessageDto.getUserId())
        .handle(
            ((queueId, sink) -> {
              if (!ObjectUtils.isEmpty(queueId)) {
                sink.error(
                    new UserNotAvailableForQueueException(
                        "User has a queue request in progress already, current queueId:"
                            + queueId));
              }
            }))
        .then(
            Mono.just(
                globalMapper.queueRequestMessageDtoToQueueWithFindingState(queueRequestMessageDto)))
        .flatMap(queue -> queuesRepository.save(queue, DEFAULT_TTL))
        .flatMap(aboolean -> messageProducerService.send(queueRequestMessageDto))
        .then(
            Mono.just(
                globalMapper.queueRequestMessageDtoToQueueCreatedResponsePayload(
                    queueRequestMessageDto)))
        .doOnError(
            throwable -> {
              log.error("Failed to create Queue due to {}", throwable.getMessage());
            })
        .doOnSuccess(
            queueCreatedResponsePayload -> {
              log.info(
                  "Successfully sent message to Q and saved state to repository for userId {} and queueId {}",
                  queueRequestMessageDto.getUserId(),
                  queueRequestMessageDto.getQueueId());
            });
  }

  public Mono<QueueStatusResponsePayload> getQueueStatus(String queueId) {
    return queuesRepository
        .get(queueId)
        .switchIfEmpty(
            Mono.error(new QueueNotFoundException("The queueId " + queueId + " was not found")))
        .flatMap(queue -> Mono.just(globalMapper.queueToQueueStatusResponsePayload(queue)))
        .doFirst(() -> log.info("Retrieving queuestatus for queueId {}", queueId))
        .doOnSuccess(
            queueStatusResponsePayload ->
                log.info("Queuestatus for queueId {} is {}", queueId, queueStatusResponsePayload));
  }

  public Mono<Boolean> deleteQueue(String queueId) {
    return queuesRepository.delete(queueId);
  }

  public Mono<Boolean> setQueueStatusToMatchForMatchId(String queueId, String matchId) {
    return queuesRepository
        .get(queueId)
        .map(queue -> queue.toBuilder().matchId(matchId).queueStatus(QueueStatus.MATCHED).build())
        .flatMap(q -> queuesRepository.save(q, DEFAULT_TTL))
        .doFirst(() -> log.info("Setting queueId {} to Matched for matchId {}", queueId, matchId));
  }
}
