package com.riotgames.interview.queueservice.services;

import com.riotgames.interview.queueservice.dtos.QueueRequestMessageDto;
import jakarta.annotation.PreDestroy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class MessageProducerService {

  private final ReactiveKafkaProducerTemplate<String, QueueRequestMessageDto>
      reactiveKafkaProducerTemplate;

  private final String topic;

  public MessageProducerService(
      @Autowired
          ReactiveKafkaProducerTemplate<String, QueueRequestMessageDto>
              reactiveKafkaProducerTemplate,
      @Value("${REQUEST_QUEUE_TOPIC}") String topic) {
    this.reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate;
    this.topic = topic;
  }

  public Mono<Void> send(QueueRequestMessageDto queueRequestMessageDto) {
    return reactiveKafkaProducerTemplate
        .send(topic, queueRequestMessageDto.getUserId(), queueRequestMessageDto)
        .flatMap(voidSenderResult -> Mono.empty());
  }

  @PreDestroy
  public void closeSender() {
    reactiveKafkaProducerTemplate.close();
  }
}
