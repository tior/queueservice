package com.riotgames.interview.queueservice.controllers;

import com.riotgames.interview.queueservice.dtos.QueueCreatedResponsePayload;
import com.riotgames.interview.queueservice.dtos.QueueRequestPayload;
import com.riotgames.interview.queueservice.dtos.QueueStatusResponsePayload;
import com.riotgames.interview.queueservice.exceptions.QueueNotFoundException;
import com.riotgames.interview.queueservice.services.QueueService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/queues")
@Slf4j
public class QueueController {
  /** This controller handles matchmaking queue requests to 1. Join 2. Get status 3. Cancel */
  private final QueueService queueService;

  public QueueController(@Autowired QueueService queueService) {
    this.queueService = queueService;
  }

  @PostMapping
  public Mono<QueueCreatedResponsePayload> createQueueRequest(
      @Valid @RequestBody Mono<QueueRequestPayload> queueRequestPayload) {
    return queueRequestPayload
        .flatMap(
            requestPayload ->
                queueService
                    .createQueue(requestPayload)
                    .doFirst(
                        () ->
                            log.info(
                                "Received request to join queue for {}",
                                requestPayload.getUserId())))
        .doOnSuccess(
            responsePayload ->
                log.info(
                    "Successfully added user {} with queueId {}",
                    responsePayload.getUserId(),
                    responsePayload.getQueueId()));
  }

  @GetMapping("/{queueId}")
  public Mono<QueueStatusResponsePayload> getQueueStatus(@PathVariable("queueId") String queueId) {
    return queueService
        .getQueueStatus(queueId)
        .doFirst(() -> log.info("Received request to get queue status for {}", queueId))
        .doOnSuccess(
            queueStatusResponsePayload ->
                log.info(
                    "Queue status for {} is currently {}",
                    queueStatusResponsePayload.getQueueId(),
                    queueStatusResponsePayload.getQueueStatus()))
        .onErrorResume(
            QueueNotFoundException.class,
            t -> {
              log.error("QueueId {} not found", queueId);
              return Mono.error(
                  new ResponseStatusException(HttpStatus.NOT_FOUND, "QueueId not found", t));
            });
  }

  @DeleteMapping("/{queueId}")
  public Mono<Boolean> cancelQueueRequest(@PathVariable("queueId") String queueId) {
    return queueService
        .deleteQueue(queueId)
        .doFirst(() -> log.info("Received request to delete queue {}", queueId));
  }
}
