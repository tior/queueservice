package com.riotgames.interview.queueservice.mappers;

import com.riotgames.interview.queueservice.dtos.QueueCreatedResponsePayload;
import com.riotgames.interview.queueservice.dtos.QueueRequestMessageDto;
import com.riotgames.interview.queueservice.dtos.QueueRequestPayload;
import com.riotgames.interview.queueservice.dtos.QueueStatusResponsePayload;
import com.riotgames.interview.queueservice.enums.QueueStatus;
import com.riotgames.interview.queueservice.models.Queue;
import java.time.OffsetDateTime;
import java.util.UUID;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface GlobalMapper {
  default QueueRequestMessageDto queueRequestPayloadToqueueRequestMessageDto(
      QueueRequestPayload queueRequestPayload) {
    return QueueRequestMessageDto.builder()
        .rank(queueRequestPayload.getRank())
        .gameType(queueRequestPayload.getGameType())
        .queueId(UUID.randomUUID().toString())
        .userId(queueRequestPayload.getUserId())
        .createdDateTime(OffsetDateTime.now())
        .queueId(UUID.randomUUID().toString())
        .build();
  }

  default Queue queueRequestMessageDtoToQueueWithFindingState(
      QueueRequestMessageDto queueRequestMessageDto) {
    return Queue.builder()
        .queueStatus(QueueStatus.FINDING)
        .rank(queueRequestMessageDto.getRank())
        .gameType(queueRequestMessageDto.getGameType())
        .queueId(queueRequestMessageDto.getQueueId())
        .userId(queueRequestMessageDto.getUserId())
        .createdDateTime(queueRequestMessageDto.getCreatedDateTime())
        .updatedDateTime(queueRequestMessageDto.getCreatedDateTime())
        .build();
  }

  QueueCreatedResponsePayload queueRequestMessageDtoToQueueCreatedResponsePayload(
      QueueRequestMessageDto queueCreatedResponsePayload);

  QueueStatusResponsePayload queueToQueueStatusResponsePayload(Queue queue);
}
