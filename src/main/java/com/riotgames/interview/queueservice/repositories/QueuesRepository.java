package com.riotgames.interview.queueservice.repositories;

import com.riotgames.interview.queueservice.enums.QueueStatus;
import com.riotgames.interview.queueservice.exceptions.QueueNotFoundException;
import com.riotgames.interview.queueservice.exceptions.QueueServiceExceptions;
import com.riotgames.interview.queueservice.models.Queue;
import java.time.Duration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
@Slf4j
public class QueuesRepository {
  private final String USERS_IN_MATCH_MAKING_QUEUE = "usersInMatchMakingQueue";

  public QueuesRepository(
      @Autowired @Qualifier("reactiveRedisTemplateForQueueObjects")
          ReactiveRedisOperations<String, Queue> reactiveRedisOperationsForQueueObjects,
      @Autowired @Qualifier("reactiveRedisTemplateForStringObjects")
          ReactiveRedisOperations<String, String> reactiveRedisOperationsForStringObjects) {
    this.reactiveRedisOperationsForQueueObjects = reactiveRedisOperationsForQueueObjects;
    this.reactiveRedisOperationsForStringObjects = reactiveRedisOperationsForStringObjects;
  }

  private final ReactiveRedisOperations<String, Queue> reactiveRedisOperationsForQueueObjects;
  private final ReactiveRedisOperations<String, String> reactiveRedisOperationsForStringObjects;

  /**
   * We need to track the current queue that's tied to the user and also the state of the queue
   * hence the duplicate
   *
   * @param queue this is the queue object that contains all the information for the incoming queue
   *     request
   * @param duration ttl
   * @return Mono<True> if save is successful, else false
   */
  public Mono<Boolean> save(Queue queue, Duration duration) {
    return this.reactiveRedisOperationsForStringObjects
        .opsForHash()
        .put(USERS_IN_MATCH_MAKING_QUEUE, queue.getUserId(), queue.getQueueId())
        .flatMap(
            aBoolean ->
                this.reactiveRedisOperationsForQueueObjects
                    .opsForValue()
                    .set(queue.getQueueId(), queue, duration))
        .doOnSuccess(
            aBoolean ->
                log.info(
                    "Successfully saved queue {} for user{}",
                    queue.getQueueId(),
                    queue.getUserId()));
  }

  public Mono<String> getUserQueue(String userId) {
    return this.reactiveRedisOperationsForStringObjects
        .<String, String>opsForHash()
        .get(USERS_IN_MATCH_MAKING_QUEUE, userId);
  }

  public Mono<Queue> get(String queueId) {
    return this.reactiveRedisOperationsForQueueObjects.opsForValue().get(queueId);
  }

  public Mono<Boolean> delete(String queueId) {
    return this.reactiveRedisOperationsForQueueObjects
        .opsForValue()
        .get(queueId)
        .switchIfEmpty(
            Mono.error(new QueueNotFoundException("The queueId " + queueId + " was not found")))
        .<Queue>handle(
            ((queue, sink) -> {
              if (queue.getQueueStatus() == QueueStatus.CANCELLED
                  || queue.getQueueStatus() == QueueStatus.MATCHED) {
                sink.error(
                    new QueueNotFoundException(
                        "The queueId "
                            + queueId
                            + " was is already in "
                            + queue.getQueueStatus()
                            + "state"));
              } else {
                sink.next(queue);
              }
            }))
        .flatMap(
            queue ->
                reactiveRedisOperationsForQueueObjects
                    .opsForValue()
                    .set(
                        queue.getQueueId(),
                        queue.toBuilder().queueStatus(QueueStatus.CANCELLED).build())
                    .flatMap(
                        aBoolean ->
                            aBoolean
                                ? Mono.just(queue.getUserId())
                                : Mono.error(
                                    new QueueServiceExceptions(
                                        "failed to delete queue" + queue.getQueueId()))))
        .flatMap(
            userId ->
                reactiveRedisOperationsForStringObjects
                    .opsForHash()
                    .remove(USERS_IN_MATCH_MAKING_QUEUE, userId))
        .flatMap(aLong -> Mono.just(true));
  }
}
