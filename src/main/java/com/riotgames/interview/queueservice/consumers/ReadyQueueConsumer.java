package com.riotgames.interview.queueservice.consumers;

import com.riotgames.interview.queueservice.dtos.MatchedPlayersMessageDto;
import com.riotgames.interview.queueservice.dtos.QueueRequestMessageDto;
import com.riotgames.interview.queueservice.services.QueueService;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import java.time.Duration;
import java.util.List;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class ReadyQueueConsumer {
  private final ReactiveKafkaConsumerTemplate<String, MatchedPlayersMessageDto>
      reactiveKafkaConsumerTemplate;
  private final QueueService queueService;

  public ReadyQueueConsumer(
      ReactiveKafkaConsumerTemplate<String, MatchedPlayersMessageDto> reactiveKafkaConsumerTemplate,
      QueueService queueService) {
    this.reactiveKafkaConsumerTemplate = reactiveKafkaConsumerTemplate;
    this.queueService = queueService;
  }

  @PostConstruct
  public void consume() {
    reactiveKafkaConsumerTemplate
        .receive()
        .doOnNext(
            stringMatchedPlayersMessageDtoReceiverRecord ->
                log.info("received {} message", stringMatchedPlayersMessageDtoReceiverRecord))
        .map(ConsumerRecord::value)
        .flatMap(this::setPlayersStatusToMatched) // Use flatMap instead of map
        .subscribe();
  }

  private Mono<Void> setPlayersStatusToMatched(MatchedPlayersMessageDto matchedPlayersMessageDto) {
    log.info("Processing match {}", matchedPlayersMessageDto.getMatchId());
    List<QueueRequestMessageDto> listOfPlayers =
        Stream.concat(
                matchedPlayersMessageDto.getTeam1Players().stream(),
                matchedPlayersMessageDto.getTeam2Players().stream())
            .toList();
    log.info("Merged all players into a single list {}", listOfPlayers);

    return Flux.fromIterable(listOfPlayers)
        .flatMap(
            queueRequestMessageDto ->
                queueService
                    .setQueueStatusToMatchForMatchId(
                        queueRequestMessageDto.getQueueId(), matchedPlayersMessageDto.getMatchId())
                    .doOnSuccess(
                        unused ->
                            log.info(
                                "Successfully set queueId {} to Matched for playerId {}",
                                queueRequestMessageDto.getQueueId(),
                                queueRequestMessageDto.getUserId())))
        .then();
  }

  @PreDestroy
  public void closeConsumer() {
    reactiveKafkaConsumerTemplate
        .assignment()
        .delayElements(Duration.ofSeconds(20))
        .flatMap(reactiveKafkaConsumerTemplate::pause)
        .subscribe();
  }
}
