package com.riotgames.interview.queueservice.models;

import com.riotgames.interview.queueservice.enums.GameType;
import com.riotgames.interview.queueservice.enums.QueueStatus;
import com.riotgames.interview.queueservice.enums.Rank;
import java.io.Serializable;
import java.time.OffsetDateTime;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;
import org.springframework.data.annotation.Id;

@Builder(toBuilder = true)
@Getter
@Jacksonized
@ToString
public class Queue implements Serializable {

  @Id private String userId;
  private String queueId;
  private Rank rank;
  private GameType gameType;
  private QueueStatus queueStatus;
  private OffsetDateTime createdDateTime;
  private OffsetDateTime updatedDateTime;
  private String matchId;
}
