package com.riotgames.interview.queueservice.dtos;

import com.riotgames.interview.queueservice.enums.GameType;
import com.riotgames.interview.queueservice.enums.QueueStatus;
import com.riotgames.interview.queueservice.enums.Rank;
import java.time.OffsetDateTime;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Builder
@Getter
@Slf4j
@ToString
public class QueueStatusResponsePayload {
  private String userId;
  private String queueId;
  private Rank rank;
  private GameType gameType;
  private OffsetDateTime createdDateTime;
  private OffsetDateTime updatedDateTime;
  private QueueStatus queueStatus;
  private String matchId;
}
