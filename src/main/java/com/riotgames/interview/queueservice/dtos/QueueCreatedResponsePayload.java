package com.riotgames.interview.queueservice.dtos;

import com.riotgames.interview.queueservice.enums.GameType;
import com.riotgames.interview.queueservice.enums.Rank;
import java.time.OffsetDateTime;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;

@Builder
@Getter
@Jacksonized
@ToString
public class QueueCreatedResponsePayload {
  private String userId;
  private String queueId;
  private Rank rank;
  private GameType gameType;
  private OffsetDateTime createdDateTime;
}
