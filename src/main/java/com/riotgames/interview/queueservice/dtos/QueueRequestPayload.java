package com.riotgames.interview.queueservice.dtos;

import com.riotgames.interview.queueservice.enums.GameType;
import com.riotgames.interview.queueservice.enums.Rank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;

@Builder
@Jacksonized
@Getter
@ToString
public class QueueRequestPayload {
  @NotEmpty(message = "user id empty")
  private String userId;

  @NotNull(message = "rank is required")
  private Rank rank;

  @NotNull(message = "gameType is required")
  private GameType gameType;
}
