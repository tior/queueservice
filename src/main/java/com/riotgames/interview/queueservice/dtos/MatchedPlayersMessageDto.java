package com.riotgames.interview.queueservice.dtos;

import com.riotgames.interview.queueservice.enums.GameType;
import com.riotgames.interview.queueservice.enums.Rank;
import java.time.OffsetDateTime;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;

@Builder
@Getter
@Jacksonized
@ToString
public class MatchedPlayersMessageDto {
  private String matchId;
  private List<QueueRequestMessageDto> team1Players;
  private List<QueueRequestMessageDto> team2Players;
  private Rank rank;
  private GameType gameType;
  private OffsetDateTime createdDateTime;
}
