package com.riotgames.interview.queueservice.exceptions;

public class QueueNotFoundException extends QueueServiceExceptions {

  public QueueNotFoundException(String message) {
    super(message);
  }

  public QueueNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }
}
