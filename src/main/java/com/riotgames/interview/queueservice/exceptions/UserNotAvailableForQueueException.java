package com.riotgames.interview.queueservice.exceptions;

public class UserNotAvailableForQueueException extends QueueServiceExceptions {

  public UserNotAvailableForQueueException(String message) {
    super(message);
  }

  public UserNotAvailableForQueueException(String message, Throwable cause) {
    super(message, cause);
  }
}
