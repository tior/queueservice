package com.riotgames.interview.queueservice.exceptions;

public class QueueServiceExceptions extends RuntimeException {

  public QueueServiceExceptions(String message) {
    super(message);
  }

  public QueueServiceExceptions(String message, Throwable cause) {
    super(message, cause);
  }
}
