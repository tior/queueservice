package com.riotgames.interview.queueservice.configs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.riotgames.interview.queueservice.models.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class ReactiveRedisConfiguration {
  @Bean("reactiveRedisTemplateForQueueObjects")
  public ReactiveRedisTemplate<String, Queue> reactiveRedisTemplateForQueueObjects(
      ReactiveRedisConnectionFactory factory, ObjectMapper objectMapper) {
    StringRedisSerializer keySerializer = new StringRedisSerializer();

    Jackson2JsonRedisSerializer<Queue> valueSerializer =
        new Jackson2JsonRedisSerializer<>(objectMapper, Queue.class);

    RedisSerializationContext.RedisSerializationContextBuilder<String, Queue> builder =
        RedisSerializationContext.newSerializationContext(keySerializer);

    RedisSerializationContext<String, Queue> context = builder.value(valueSerializer).build();

    return new ReactiveRedisTemplate<>(factory, context);
  }

  @Bean("reactiveRedisTemplateForStringObjects")
  public ReactiveRedisTemplate<String, String> reactiveRedisTemplateForStringObjects(
      ReactiveRedisConnectionFactory factory) {
    StringRedisSerializer keySerializer = new StringRedisSerializer();
    StringRedisSerializer valueSerializer = new StringRedisSerializer();

    RedisSerializationContext.RedisSerializationContextBuilder<String, String> builder =
        RedisSerializationContext.newSerializationContext(keySerializer);
    RedisSerializationContext<String, String> context = builder.value(valueSerializer).build();

    return new ReactiveRedisTemplate<>(factory, context);
  }
}
