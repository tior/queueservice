package com.riotgames.interview.queueservice.configs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.riotgames.interview.queueservice.dtos.MatchedPlayersMessageDto;
import java.util.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import reactor.kafka.receiver.ReceiverOptions;

@Configuration
@Slf4j
public class ReactiveKafkaConsumerConfiguration {

  @Bean
  public ReceiverOptions<String, MatchedPlayersMessageDto> kafkaReceiverOptions(
      ObjectMapper objectMapper,
      @Value(value = "${READY_Q_TOPIC}") String topic,
      KafkaProperties kafkaProperties) {
    ReceiverOptions<String, MatchedPlayersMessageDto> basicReceiverOptions =
        ReceiverOptions.create(kafkaProperties.buildConsumerProperties());
    JsonDeserializer<MatchedPlayersMessageDto> jsonDeserializer =
        new JsonDeserializer<>(MatchedPlayersMessageDto.class);
    jsonDeserializer.trustedPackages("*");
    jsonDeserializer.ignoreTypeHeaders();
    return basicReceiverOptions
        .subscription(Collections.singletonList(topic))
        .withValueDeserializer(jsonDeserializer);
  }

  @Bean
  public ReactiveKafkaConsumerTemplate<String, MatchedPlayersMessageDto>
      reactiveKafkaConsumerTemplate(
          ReceiverOptions<String, MatchedPlayersMessageDto> kafkaReceiverOptions) {
    return new ReactiveKafkaConsumerTemplate<>(kafkaReceiverOptions);
  }
}
