package com.riotgames.interview.queueservice.configs;

import com.riotgames.interview.queueservice.dtos.QueueRequestMessageDto;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;
import reactor.kafka.sender.SenderOptions;

@Configuration
@Slf4j
public class ReactiveKafkaProducerConfiguration {

  @Bean
  public ReactiveKafkaProducerTemplate<String, QueueRequestMessageDto>
      reactiveKafkaProducerTemplate(KafkaProperties properties) {
    Map<String, Object> props = properties.buildProducerProperties();
    return new ReactiveKafkaProducerTemplate<>(SenderOptions.create(props));
  }
}
