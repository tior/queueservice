package com.riotgames.interview.queueservice.exceptionhandlers;

import com.riotgames.interview.queueservice.exceptions.QueueServiceExceptions;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.bind.support.WebExchangeBindException;
import reactor.core.publisher.Mono;

@RestControllerAdvice
public class GlobalControllerExceptionHandler {

  @ExceptionHandler(QueueServiceExceptions.class)
  public Mono<ResponseEntity<String>> handleQueueServiceExceptions(
      QueueServiceExceptions exceptions) {
    return Mono.just(ResponseEntity.badRequest().body(exceptions.getMessage()));
  }

  @ExceptionHandler(WebExchangeBindException.class)
  public Mono<ResponseEntity<List<String>>> handleWebExchangeBindException(
      WebExchangeBindException e) {
    return Mono.just(ResponseEntity.badRequest().body(this.getAllErrorMessages(e.getAllErrors())));
  }

  private List<String> getAllErrorMessages(List<ObjectError> errorList) {
    return errorList.stream()
        .map(DefaultMessageSourceResolvable::getDefaultMessage)
        .collect(Collectors.toList());
  }
}
