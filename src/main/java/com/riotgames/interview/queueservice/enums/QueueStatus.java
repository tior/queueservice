package com.riotgames.interview.queueservice.enums;

public enum QueueStatus {
  CANCELLED,
  FINDING,
  MATCHED
}
