# Queue Service
## What

## Build requirements
1. JDK 17
2. MVN 3.6.3

## Frameworks/Utils in use
1. Spring boot 3
2. Lombok
3. git-code-format-maven-plugin

## Dependencies
1. Redis. 
   - You may use the following command to get redis running locally listening on port 6379
      ```shell
      docker run -d --name redis-stack -p 6379:6379 -p 8001:8001 redis/redis-stack:latest
      ```
   - If you have already executed the above before, you may run it again(if it's stopped)
     ```shell
     docker start redis-stack 
     ```